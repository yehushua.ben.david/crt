package crt

import (
	"fmt"
	"github.com/eiannone/keyboard"
	"golang.org/x/term"
	"math/rand"
	"os"
	"strings"
	"sync"
	"time"
)

type KeyValue uint16
type Key struct {
	Value KeyValue
	Chr   rune
}

const (
	KLeft      KeyValue = 65515
	KRight              = 65514
	KUp                 = 65517
	KDown               = 65516
	KEscape             = 27
	KReturn             = 13
	KBackSpace          = 127
	KCtlC               = 3
	KSpace              = 32
)

type KeyEventHandler func(k Key)
type KeyEventHandlerId [16]byte

var KeyEventHandlers map[KeyEventHandlerId]KeyEventHandler
var KeyEventHandlersMutex = sync.Mutex{}

func Xstr(str string, count int) string {
	rst := make([]byte, 0, len(str)*count)
	bStr := []byte(str)
	for i := 0; i < count; i++ {
		rst = append(rst, bStr...)
	}
	return string(rst)
}
func (k *KeyEventHandlerId) New() {
	rand.Seed(time.Now().UnixNano())
	for i, _ := range *k {
		(*k)[i] = byte(rand.Uint32() % 256)
	}
}

type Screen struct {
	X, Y, W, H    int
	BgR, BgG, BgB int
	FgR, FgG, FgB int
	UseColor      bool
	buff          []string
}

func (s *Screen) Cls() *Screen {
	return s.Fill(' ')
}
func (s *Screen) Fill(r rune) *Screen {
	for i := 0; i < s.H; i++ {
		if s.UseColor {
			W(s.X, s.Y+i, ColB(s.BgR, s.BgG, s.BgB, ColF(s.FgR, s.FgG, s.FgB,
				Xstr(string(r), s.W))))
		} else {
			W(s.X, s.Y+i, Xstr(string(r), s.W))
		}
	}
	MoveNLeft(s.X + s.W)
	MoveDown()
	return s
}
func (s *Screen) Draw() *Screen {
	s.Cls()
	for i, ln := range s.buff {
		line := ln + Xstr(" ", s.W)
		if s.UseColor {
			W(s.X, s.Y+i,
				ColB(s.BgR, s.BgG, s.BgB, ColF(s.FgR, s.FgG, s.FgB,
					line[:s.W])))
		} else {
			W(s.X, s.Y+i,line[:s.W])
		}
	}
	MoveNLeft(s.X + s.W)
	MoveDown()
	return s
}
func (s *Screen) AddText(ln string) *Screen {
	lines := strings.Split(ln, "\n")
	if len(lines) > 1 {
		for _, l := range lines {
			s.AddText(l)
		}
		return s
	}
	if len(ln) > s.W {
		s.AddText(ln[:s.W])
		s.AddText(ln[s.W:])
		return s
	}
	s.buff = append(s.buff, ln)
	if len(s.buff) > s.H {
		s.buff = s.buff[1:]
	}
	s.Draw()
	return s
}

func NewScreen(x, y, w, h int) *Screen {
	return &Screen{
		X:    x,
		Y:    y,
		W:    w,
		H:    h,
		FgR:  255,
		FgG:  255,
		FgB:  255,
		buff: make([]string, 0),
	}
}
func init() {
	KeyEventHandlers = map[KeyEventHandlerId]KeyEventHandler{
		KeyEventHandlerId{}: func(k Key) {
			if k.Chr == 0 && k.Value == 3 {
				panic("BREAK")
			}
		},
	}
	go func() {
		for {
			c, k, e := keyboard.GetSingleKey()
			if k == KSpace {
				c = ' '
			}
			if k == KReturn {
				c = '\n'
			}
			if e == nil {
				KeyEventHandlersMutex.Lock()
				for _, m := range KeyEventHandlers {
					go m(Key{Value: KeyValue(k), Chr: c})
				}
				KeyEventHandlersMutex.Unlock()
			}
		}
	}()
}

func WaitKey() Key {
	aKey := make(chan Key)
	var id KeyEventHandlerId
	id = RegisterKeyEventHandler(func(k Key) {
		UnRegisterKeyEventHandler(id)
		aKey <- k
	})
	return <-aKey
}

func GetSize() (width, height int, err error) {
	return term.GetSize(int(os.Stdin.Fd()))
}
func UnRegisterKeyEventHandler(id KeyEventHandlerId) {
	KeyEventHandlersMutex.Lock()
	defer KeyEventHandlersMutex.Unlock()
	delete(KeyEventHandlers, id)
}
func RegisterKeyEventHandler(keh func(k Key)) KeyEventHandlerId {
	k := KeyEventHandlerId{}
	k.New()
	KeyEventHandlersMutex.Lock()
	defer KeyEventHandlersMutex.Unlock()
	KeyEventHandlers[k] = keh
	return k
}
func SavePos() {
	print("\033[s")
}
func MoveUp() {
	print("\033[A")
}

func MoveLeft() {
	print("\033[D")
}

func MoveRight() {
	print("\033[C")
}

func MoveDown() {
	print("\033[B")
}

func MoveNUp(n int) {
	if n == 0 {
		return
	}
	print(fmt.Sprintf("\033[%dA", n))
}

func MoveNLeft(n int) {
	if n == 0 {
		return
	}
	print(fmt.Sprintf("\033[%dD", n))
}

func MoveNRight(n int) {
	if n == 0 {
		return
	}
	print(fmt.Sprintf("\033[%dC", n))
}

func MoveNDown(n int) {
	if n == 0 {
		return
	}
	print(fmt.Sprintf("\033[%dB", n))
}

func RestorePos() {
	print("\033[u")
}
func Readln() string {
	return ReadlnDefault("")
}
func ReadlnDefault(value string) string {
	rst := value
	pos := len(value)
	print(rst + " ")
	MoveNLeft(len(rst) + 1)
	MoveNRight(pos)

	for {
		oldPos := pos
		k := WaitKey()
		if k.Value == KReturn {
			return rst
		}
		if k.Value == KBackSpace {
			if pos > 0 {
				rst = rst[:pos-1] + rst[pos:]
				pos--
			}
		}
		if k.Chr != 0 {
			rst = rst[:pos] + string(k.Chr) + rst[pos:]
			pos++
		}
		if k.Value == KLeft {
			if pos > 0 {
				pos--
			}
		}
		if k.Value == KRight {
			if pos < len(rst) {
				pos++
			}
		}
		MoveNLeft(oldPos)
		print(rst + " ")
		MoveNLeft(len(rst) + 1)
		MoveNRight(pos)
	}
}
func Bold(str string) string {
	return fmt.Sprintf("\033[1m%s\033[m", str)
}
func ColF(r, g, b int, str string) string {
	return fmt.Sprintf("\033[38;2;%d;%d;%dm%s\033[m", r, g, b, str)
}
func ColB(r, g, b int, str string) string {
	return fmt.Sprintf("\033[48;2;%d;%d;%dm%s\033[m", r, g, b, str)
}
func Str(oo ...interface{}) string {
	rst := ""
	for _, o := range oo {
		rst += fmt.Sprintf("%+v", o)
	}
	return rst
}
func Cls() {
	print("\033[1;1H\033[J")
}

func MoveTo(x, y int) {
	print(fmt.Sprintf("\033[%d;%dH", y+1, x+1))
}

func HideCursor() {
	print("\033[?25l")
}
func ShowCursor() {
	print("\033[?25h")
}
func W(x, y int, s string) {
	print(fmt.Sprintf("\033[%d;%dH%s", y+1, x+1, s))
}

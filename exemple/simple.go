package main

import (
	"fmt"
	"gitlab.com/yehushua.ben.david/crt"
)

func Form() {
	crt.Cls()
	//pos:=0
	w,h,_ := crt.GetSize()
	//fw:=30
	for i:=1 ; i<w-1 ; i++ {
		crt.W(i,0,"_")
		crt.W(i,h-1,"_")
	}

	for i:=1 ; i<h-1 ; i++ {
		crt.W(0,i,"|")
		crt.W(w-1,i,"|")
	}
	crt.W(0,h-1 , "|")
	crt.W(w-1,h-1 , "|")
	crt.MoveTo(1,1)

}

func main() {
	fmt.Println("Simple commander", crt.Bold("type help"))
	w,h,_:=crt.GetSize()
	src := crt.NewScreen(w/2,3,w/4,h/3)
	src.Fill('*')
	crt.ShowCursor()
	for {
		fmt.Print(">")
		s := crt.Readln()
		fmt.Println()
		if s != "" {
			fmt.Println(s)
			src.AddText(s)
		}
		if s == "cls" {
			crt.Cls()
			src.Draw()
		}
		if s == "exit" {
			break
		}
		if s == "form" {
			Form()
		}
		if s == "help" {
			fmt.Println(`cls	Clear screen
help	Print this
exit	Exit`)
		}

	}
}

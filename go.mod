module gitlab.com/yehushua.ben.david/crt

go 1.16

require (
	github.com/eiannone/keyboard v0.0.0-20200508000154-caf4b762e807
	golang.org/x/term v0.0.0-20210927222741-03fcf44c2211
)
